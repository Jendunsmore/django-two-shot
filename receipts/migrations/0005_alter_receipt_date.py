# Generated by Django 5.0 on 2023-12-18 20:50

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0004_alter_receipt_account_alter_receipt_date"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="date",
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
