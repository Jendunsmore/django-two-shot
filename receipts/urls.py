from django.urls import path
from receipts.views import receipt_list

def redirect_to_receipt_list(request):
    return receipt_list("home")

urlpatterns = [
    path("", receipt_list, name="home"),
]
